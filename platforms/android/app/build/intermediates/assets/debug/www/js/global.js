document.getElementById("asd").addEventListener('click', getPhoto);

function errorCamera(e) {
    console.error('- Error: ' + e);
}

function onPhotoDataSuccess(imageData) {
    // Get image handle
    //
    var smallImage = document.getElementById("image");

    smallImage.src = "data:image/jpeg;base64," + imageData;
    console.log(imageData)
}

function getPhoto() {
    navigator.camera.getPicture(onPhotoDataSuccess, errorCamera, settingsFromCamera);
}

function sendConfirm(message) {
    navigator.notification.confirm(message, confirmCallback, 'Medicare', ['Ok', 'Cancel']);
}

function confirmCallback(index) {
    alert('Pulsaste: ' + (index == 0) ? 'Ok' : 'Cancel');
}