var destinationType;
var pictureSource;
var settingsFromCamera;


document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    console.log("Device ready");
    
    pictureSource = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;

    settingsFromCamera = {
        quality: 50,
        destinationType: destinationType.DATA_URL,
        sourceType: pictureSource.CAMERA
    };
}